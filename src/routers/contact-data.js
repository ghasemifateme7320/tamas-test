const express = require('express');
const contactRepository = require("../repositories/contact-data");
const router = express.Router();


const addNewContact = async (req, res) => {
    try {
        await contactRepository.createContact(req.body);
        res.json({"message": "book successfully added"})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};


const updateContact = async (req, res) => {
    try {
        const result = await contactRepository.updateContact(req.body);
        res.json({"message": result + " updated"})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};


const searchContactByDetails = async (req, res) => {
    try {
        let beginNum = parseInt(req.query.begin) || 0;
        let totalNum = parseInt(req.query.total) || 10;
        let details = req.query.details;
        const result = await contactRepository.searchContactByDetails(details, beginNum, totalNum);
        res.json({"message": "your search result is: ", books: result})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};


const returnAllContact = async (req, res) => {
    try {
        let beginNum = parseInt(req.query.begin) || 0;
        let totalNum = parseInt(req.query.total) || 10;
        const result = await contactRepository.returnAllContact(beginNum, totalNum);
        res.json({"message": "your search result is: " + result})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
}


const searchContactController = async (req, res) => {
    try {
        if (req.query.details !== undefined) {
            const result = await searchContactByDetails(req, res);
            console.log("contact result:", result)
        } else {
            const result = await returnAllContact(req, res);
            console.log("contact result:", result)
        }
    } catch (e) {
        res.json({'message': e.message});
    }
};


router.post('/', addNewContact);
router.put('/', updateContact);
router.get('/', searchContactController);
module.exports = router;