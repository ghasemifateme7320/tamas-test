const express = require('express');
const userRepository = require("../repositories/user");
const router = express.Router();


const createNewUser = async (req, res) => {
    try {
        const userData = await userRepository.createUser(req.body);
        res.json({message: userData.username + ' registered successfully'})
    } catch (e) {
        res.status(500).json({message: e.message})
    }
};

router.post('/', createNewUser);
