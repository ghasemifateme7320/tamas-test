const mongoose = require('mongoose');

let contactSchema = new mongoose.Schema({
    "phoneNumber"        : {type: String, unique: true, require: true},
    "firstName"         : String,
    "lastName"          : String,
});
module.exports = {
    contactModel: mongoose.model('contact', contactSchema, 'contact'),
};