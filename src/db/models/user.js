const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    "username"          : {type: String, unique: true, require: true}
});
module.exports = {
    userModel: mongoose.model('user', userSchema, 'user'),
};