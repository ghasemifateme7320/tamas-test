const contactSchema = require('../db/models/contact-data').contactModel;


const createContact = async (data) => {
    try {
        const result = await contactSchema.findOneAndUpdate(
            {
                "phoneNumber"        : data.phoneNumber,
                "firstName"         : data.firstName,
                "lastName"          : data.lastName,
            },
            data,
            {upsert: true}
        )
        return result
    } catch (e) {
        console.log("createContact ERROR: ", e.message)
    }
};


const searchContactByDetails = async (details, begin, total) => {
    return await contactSchema.find({$text: {$search: details}}).sort({"title": -1}).skip(begin).limit(total)
};


const returnAllContact = async (begin, total) => {
    return await contactSchema.find({}).sort({"title": -1}).skip(begin).limit(total)
};


const updateContact = async (data) => {
    return await contactSchema.findOneAndUpdate(
        {
            "phoneNumber"        : data.phoneNumber,
            "firstName"         : data.firstName,
            "lastName"          : data.lastName,
        },
        data
    )
};

module.exports = {
    updateContact,
    returnAllContact,
    searchContactByDetails,
    createContact

};