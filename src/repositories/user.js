const userSchema = require('../db/models/user').userModel;

const createUser = (userData) => {
    return userSchema.findOneAndUpdate(
        {
            "username": userData.username
        },
        userData,
        {upsert: true}
    )
};

module.exports = {createUser}